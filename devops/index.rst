*****************************
拍拍贷DevOps体系规划
*****************************

.. _preface::

.. toctree::
   :maxdepth: 2
   :caption: 前言

   p0-preface/preface
   p0-preface/supercell

.. _intro::

.. toctree::
   :maxdepth: 2
   :caption: 第一部分 DevOps-概念篇

   p1-introduction/c11-introduction
   p1-introduction/c12-value
   
   
.. _arch::

.. toctree::
   :maxdepth: 2
   :caption: 第二部分 DevOps-架构篇

   p2-arch/c21-overview
   p2-arch/c22-arch-group
   p2-arch/c23-arch-tech
   p2-arch/c24-arch-process
   p2-arch/c25-arch-culture
   
.. cloud::

.. toctree::
   :maxdepth: 2
   :caption: 第三部分 DevOps-云计算篇

   p3-cloud/c31-overview
   p3-cloud/c32-cloud-is-safe
   p3-cloud/c33-cloud-cost

.. docker::

.. toctree::
   :maxdepth: 2
   :caption: 第四部分 DevOps-Docker篇

   p4-docker/c41-what-is-docker
   p4-docker/c42-why-use-docker
   p4-docker/c43-how-to-use-docker
   
.. microservice::

.. toctree::
   :maxdepth: 2
   :caption: 第五部分 DevOps- 微服务篇

   p5-microservice/c51-what-is-microservice
   p5-microservice/c52-why-use-microservice
   p5-microservice/c53-how-to-implement-microservice
   