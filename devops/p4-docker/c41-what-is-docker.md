# Docker是什么 - 解决什么问题

在传统IT运维中，软件的构建分发和运行是其重要的职责之一，同是由于环境，软件依赖等复杂性，IT运维一直在探寻更加高效的解决方案，直到docker的出现。 

## Docker基础知识

Docker 是一个开源的应用容器引擎，让开发者可以打包他们的应用以及依赖包到一个可移植的容器中，然后发布到任何流行的 Linux 机器上，也可以实现虚拟化。容器是完全使用沙箱机制，相互之间不会有任何接口。

* CPU隔离：cgroup.cpuset,cpu
* 内存隔离： cgroup.memory
* 进程隔离： pid namespace
* 文件系统隔离： chroot
* 磁盘空间隔离： 无
* 磁盘IO隔离：无
* 网络带宽隔离：无
* 镜像系统：完备
* 执行引擎： lxc,libcontainer
* 文件系统： aufs,device mapper, overlayfs
* 资源可见性：无隔离

更多知识参考： 

[Docker百度百科](http://baike.baidu.com/link?url=lbqIEo8HG2RKkgbNwBHjD9q3NkE3muYSXXdp2A-eBbke4kYxujb-NIs9Fg1MA0qe6sMdpmzbXlaJ2NIxEDMkda)

[Docker官网](https://www.docker.com/)

## Docker - 一种软件的构建分发和运行的新型技术

Docker是解决什么问题的? 
Docker的问题域是软件的构建分发和运行。从这3个方面和传统运维及内部现有运维系 统做下对比:


+------------+--------------------------+-------------+
| __生命周期__ | __传统__                 | __Docker__  |
+------------+--------------------------+-------------+
| Build      | makefile、rpm、maven、tgz | docker build|
+------------+--------------------------+-------------+
| Ship       | scp，jenkins              | docker push |
+------------+--------------------------+-------------+
| Run        | bash，jenkins             | docker run  |
+------------+--------------------------+-------------+
	
从这个对比大家看到了什么?

* 传统:分散、割裂、琐碎 
* Docker:统一、一致、完备

这里看到的仅仅是表象，Docker的强大一定要深入其内涵才能领会。后续会详细介绍Docker的两大核心技术:__容器和镜像__，以及docker的核心价值。 



